
#include "nfcaccess.h"
#include <string.h>

// colocar em estrutura
nfc_context *context;
nfc_device 	*pnd;
nfc_target  pnt;
na_device_type device_type;

const nfc_modulation nmModulations[5] = {
	{ .nmt = NMT_ISO14443A, .nbr = NBR_106 },
	{ .nmt = NMT_ISO14443B, .nbr = NBR_106 },
	{ .nmt = NMT_FELICA, .nbr = NBR_212 },
	{ .nmt = NMT_FELICA, .nbr = NBR_424 },
	{ .nmt = NMT_JEWEL, .nbr = NBR_106 }	
};

nfc_target pnt; //= {
	//~ .nm = {
		//~ .nmt = NMT_ISO14443A,
		//~ .nbr = NBR_UNDEFINED
	//~ },
	//~ .nti = {
		//~ .nai = {
			//~ .abtAtqa = { 0x00, 0x04 },
			//~ .abtUid = { 0x08, 0xab, 0xcd, 0xef },
			//~ .btSak = 0x09,
			//~ .szUidLen = 4,
			//~ .szAtsLen = 0
		//~ }			
	//~ }
//~ };

  //~ nfc_target nt = {
    //~ .nm = {
      //~ .nmt = NMT_ISO14443A,
      //~ .nbr = NBR_UNDEFINED, // Will be updated by nfc_target_init()
    //~ },
    //~ .nti = {
      //~ .nai = {
        //~ .abtAtqa = { 0x00, 0x04 },
        //~ .abtUid = { 0x08, 0x00, 0xb0, 0x0b },
        //~ .szUidLen = 4,
        //~ .btSak = 0x00,
        //~ .szAtsLen = 0,
      //~ },
    //~ }
  //~ };
  
  #define READ 		0x30
#define WRITE 		0xA2
#define SECTOR_SELECT 	0xC2

#define HALT 		0x50

static int nfcforum_tag2_io(struct nfc_emulator *emulator, const uint8_t *data_in, const size_t data_in_len, uint8_t *data_out, const size_t data_out_len){
	
  int res = 0;
  
  //inserido
	char *str;
	str_nfc_target( &str, (emulator->target), true);
	printf("\n%s\n",str);
	free(str);
 ///////////////

  uint8_t *nfcforum_tag2_memory_area = (uint8_t *)(emulator->user_data);

  printf("    In: \n");
  printf("print_hex\n");
  //~ print_hex(data_in, data_in_len);

  switch (data_in[0]) {
    case READ:
      if (data_out_len >= 16) {
        memcpy(data_out, nfcforum_tag2_memory_area + (data_in[1] * 4), 16);
        res = 16;
      } else {
        res = -ENOSPC;
      }
      break;
    case HALT:
      printf("HALT sent\n");
      res = -ECONNABORTED;
      break;
    default:
      printf("Unknown command: 0x%02x\n", data_in[0]);
      res = -ENOTSUP;
  }

  if (res < 0) {
	printf("ERR()\n");
    //~ ERR("%s (%d)", strerror(-res), -res);
  } else {
    printf("    Out: \n");printf("print_hex\n");
    //~ print_hex(data_out, res);
  }

  return res;
  
}

//~ struct nfc_emulation_state_machine state_machine = {
	//~ .io   = nfcforum_tag2_io
//~ };
//~ 
//~ static uint8_t __nfcforum_tag2_memory_area[] = {
  //~ 0x00, 0x00, 0x00, 0x00,  // Block 0
  //~ 0x00, 0x00, 0x00, 0x00, 
  //~ 0x00, 0x00, 0xFF, 0xFF,  // Block 2 (Static lock bytes: CC area and data area are read-only locked)
  //~ 0xE1, 0x10, 0x06, 0x0F,  // Block 3 (CC - NFC-Forum Tag Type 2 version 1.0, Data area (from block 4 to the end) is 48 bytes, Read-only mode)
//~ 
  //~ 0x03, 33,   0xd1, 0x02,  // Block 4 (NDEF)
  //~ 0x1c, 0x53, 0x70, 0x91,
  //~ 0x01, 0x09, 0x54, 0x02,
  //~ 0x65, 0x6e, 0x4c, 0x69,
//~ 
  //~ 0x62, 0x6e, 0x66, 0x63,
  //~ 0x51, 0x01, 0x0b, 0x55,
  //~ 0x03, 0x6c, 0x69, 0x62,
  //~ 0x6e, 0x66, 0x63, 0x2e,
//~ 
  //~ 0x6f, 0x72, 0x67, 0x00,
  //~ 0x00, 0x00, 0x00, 0x00,
  //~ 0x00, 0x00, 0x00, 0x00,
  //~ 0x00, 0x00, 0x00, 0x00
//~ };
//~ 
//~ struct nfc_emulator emulator = {
	//~ .target = &nt,
	//~ .state_machine = &state_machine,
	//~ .user_data = __nfcforum_tag2_memory_area,
//~ };



const size_t 	szModulations = 5;
const uint8_t	uiPollNr = 3;		// testar!!
const uint8_t	uiPeriod = 2;		// testar...

//~ #define MAX_FRAME_LEN 264
//~ 
//~ void stop_emulation (int sig){
  //~ 
	//~ (void)sig;
	//~ if(pnd)
		//~ nfc_abort_command(pnd);
	//~ else
		//~ exit(EXIT_FAILURE);
		//~ 
//~ }

void na_init(){
	
	nfc_init( &context );
	
	if( context == NULL ){
		
		perror("nfc_init");
		exit(EXIT_FAILURE);		
		
	}
	
	pnd = nfc_open( context, NULL);
	
	if( pnd == NULL ){
		
		perror("nfc_open");
		na_close();
		exit(EXIT_FAILURE);
		
	}
	
	//~ signal(SIGINT, stop_emulation);
	
	
	
	
	//na_set_device_type(NA_TYPE_INITIATOR);
	
}

void na_close(){
	
	if( pnd != NULL )
		nfc_close(pnd);
	
	nfc_exit(context);
	
}

//~ void na_set_device_type( na_device_type type ){
	//~ 
	//~ switch(type){
		//~ 
		//~ case NA_TYPE_INITIATOR:
		//~ 
			//~ device_type = NA_TYPE_INITIATOR;
			//~ 
			//~ if( nfc_initiator_init(pnd) != 0 ){
				//~ 
				//~ nfc_perror( pnd, "nfc_initiator_init");
				//~ na_close();
				//~ exit(EXIT_FAILURE);
				//~ 
			//~ }
			//~ 
			//~ break;
			//~ 
		//~ case NA_TYPE_TAG:
		//~ 
			//~ device_type = NA_TYPE_TAG;
			//~ 
			//~ //if( nfc_target_init(
			//~ 
			//~ break;
			//~ 
		//~ default:
			//~ break;
		//~ 
	//~ }
	//~ 
//~ }

int na_poll_tags(){
	
	int res;
	char *str;
	
	usleep(1000);
		
	printf("\n/*********************************\n *\tModo Iniciador\n *********************************/\n");
	res = nfc_initiator_poll_target( pnd, nmModulations, szModulations, uiPollNr, uiPeriod, &pnt);
	
	if( res > 0 ){
			
		str_nfc_target( &str, &pnt, true);
		printf("\n%s\n",str);
		free(str);
	}
	else
		printf("\tNenhuma Tag encontrada!\n");
	
	return res;
	
}

int na_poll_initiators(){
	
	int res;
	
	nfc_target nt = {
		.nm = {
		  .nmt = NMT_ISO14443A,
		  .nbr = NBR_UNDEFINED, // Will be updated by nfc_target_init()
		},
		.nti = {
		  .nai = {
			.abtAtqa = { 0x00, 0x04 },
			.abtUid = { 0x08, 0x00, 0xb0, 0x0b },
			.szUidLen = 4,
			.btSak = 0x00,
			.szAtsLen = 0,
		  },
		}
	};
	
	struct nfc_emulation_state_machine state_machine = {
		.io   = nfcforum_tag2_io
	};

	static uint8_t __nfcforum_tag2_memory_area[] = {
	  0x00, 0x00, 0x00, 0x00,  // Block 0
	  0x00, 0x00, 0x00, 0x00, 
	  0x00, 0x00, 0xFF, 0xFF,  // Block 2 (Static lock bytes: CC area and data area are read-only locked)
	  0xE1, 0x10, 0x06, 0x0F,  // Block 3 (CC - NFC-Forum Tag Type 2 version 1.0, Data area (from block 4 to the end) is 48 bytes, Read-only mode)

	  0x03, 33,   0xd1, 0x02,  // Block 4 (NDEF)
	  0x1c, 0x53, 0x70, 0x91,
	  0x01, 0x09, 0x54, 0x02,
	  0x65, 0x6e, 0x4c, 0x69,

	  0x62, 0x6e, 0x66, 0x63,
	  0x51, 0x01, 0x0b, 0x55,
	  0x03, 0x6c, 0x69, 0x62,
	  0x6e, 0x66, 0x63, 0x2e,

	  0x6f, 0x72, 0x67, 0x00,
	  0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00
	};

	struct nfc_emulator emulator = {
		.target = &nt,
		.state_machine = &state_machine,
		.user_data = __nfcforum_tag2_memory_area,
	};	
	
	usleep(1000);
	
	printf("\n/*********************************\n *\tModo Tag\n *********************************/\n");
	res = nfc_emulate_target( pnd, &emulator, 3000);
	
	if( res < 0 )
		printf("\tNenhum Iniciador encontrado!\n");
	
	return 0;
}

/**
 * \TODO: Alternar entre dispositivos TAG e Initiator
 */
int na_poll_devices(){
	
	na_poll_tags();
	na_poll_initiators();	
	
	return 0;	
	
}

//~ int na_target(){
	//~ 
	//~ size_t sizeTx, sizeRx;
	//~ uint8_t bufferTx[MAX_FRAME_LEN], bufferRx[MAX_FRAME_LEN];
	//~ 
	//~ 
	//~ nfc_target pnt = {
		//~ .nm = {
			//~ .nmt = NMT_ISO14443A,
			//~ .nbr = NBR_UNDEFINED
		//~ },
		//~ .nti = {
			//~ .nai = {
				//~ .abtAtqa = { 0x00, 0x04 },
				//~ .abtUid = { 0x08, 0xab, 0xcd, 0xef },
				//~ .btSak = 0x09,
				//~ .szUidLen = 4,
				//~ .szAtsLen = 0
			//~ }			
		//~ }
	//~ };
	//~ 
  //~ nfc_target nt = {
    //~ .nm = {
      //~ .nmt = NMT_ISO14443A,
      //~ .nbr = NBR_UNDEFINED, // Will be updated by nfc_target_init()
    //~ },
    //~ .nti = {
      //~ .nai = {
        //~ .abtAtqa = { 0x00, 0x04 },
        //~ .abtUid = { 0x08, 0x00, 0xb0, 0x0b },
        //~ .szUidLen = 4,
        //~ .btSak = 0x00,
        //~ .szAtsLen = 0,
      //~ },
    //~ }
  //~ };
		//~ 
	//~ 
	//~ sizeRx = nfc_target_init( pnd, &pnt, bufferRx, sizeof(bufferRx), 0);
	//~ 
	//~ printf("%u\n",sizeRx);
	//~ 
	//~ if( bufferRx[0] == 0x30 )
		//~ printf("mifare\n");
	//~ 
	//~ char str[24] = "Hello world\n";
	//~ sizeTx = strlen(str);
		//~ printf("size to send %u\n",sizeTx);
//~ 
	//~ sizeTx = nfc_target_send_bytes(pnd,str,sizeTx,0);
	//~ printf("size sended %u\n",sizeTx);
	//~ return sizeRx;
	//~ 
//~ 
//~ }



//~ void na_serv_device(){
	//~ 
	//~ nfc_target nt;
	//~ //memcpy( &nt, &pnt, sizeof(nfc_target));
	//~ 
	//~ 
	//~ 
//~ }

//~ void na_send_to_device();

//~ void na_read_from_device();
