

#ifndef _APP_H_
#define _APP_H_

#include <err.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

//~ #include "nfcaccess.h"
//~ #include "toll.h"
//~ #include "regist.h"

#define MENU_HELP "-h"
#define MENU_TOLL "-t"
#define MENU_REGI "-r"

#define RUN_MODE_DEFAULT 	0
#define RUN_MODE_TOLL	 	1
#define RUN_MODE_REGISTER 	2

#endif
