


#ifndef _TALK_H_
#define _TALK_H_

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>



#include "nfc/nfc.h"
#include "nfc/nfc-emulation.h"
//~ #include "nfc-utils.h"
//~ #include "utils/nfc-utils.h"




typedef enum { NA_TYPE_INITIATOR, NA_TYPE_TAG} na_device_type;


//~ static int nfcforum_tag2_io(struct nfc_emulator *emulator, const uint8_t *data_in, const size_t data_in_len, uint8_t *data_out, const size_t data_out_len;
void na_init();
void na_close();
int na_poll_devices();
void na_serv_device();
int na_target();

#endif
