

#ifndef _TOLL_H_
#define _TOLL_H_

#include "nfcaccess.h"
#include "clienthandler.h"

void toll_start();
void toll_poll_clients();
void toll_select_clients();

#endif
