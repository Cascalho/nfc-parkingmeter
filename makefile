

CC=gcc
CFLAGS=-c -I$(HDR_FOLDER) -lrt 
OFLAGS=-o
LFLAGS=-lpthread -lnfc

APP=nfc-mobile

SRC_FOLDER=./src/
HDR_FOLDER=./hdr

SOURCES=app.c
HEADERS=app.h nfcaccess.h toll.h regist.h clienthandler.h
OBJECTS=app.o nfcaccess.o toll.o regist.o clienthandler.o

all: $(OBJECTS)
	$(CC) $(OFLAGS) $(APP) $(OBJECTS) $(LFLAGS)
	chmod 777 $(APP)

app.o: $(SRC_FOLDER)app.c
	$(CC) $(CFLAGS) $(OFLAGS) app.o $(SRC_FOLDER)app.c
	
nfcaccess.o: $(SRC_FOLDER)nfcaccess.c
	$(CC) $(CFLAGS) $(OFLAGS) nfcaccess.o $(SRC_FOLDER)nfcaccess.c
	
toll.o: $(SRC_FOLDER)toll.c
	$(CC) $(CFLAGS) $(OFLAGS) toll.o $(SRC_FOLDER)toll.c
	
regist.o: $(SRC_FOLDER)regist.c
	$(CC) $(CFLAGS) $(OFLAGS) regist.o $(SRC_FOLDER)regist.c
	
clienthandler.o: $(SRC_FOLDER)clienthandler.c
	$(CC) $(CFLAGS) $(OFLAGS) clienthandler.o $(SRC_FOLDER)clienthandler.c
	
run: $(APP)
	./$(APP) -t
	
clean:
	rm *.o
	rm -f $(APP)
